package com.serve.DicksSportingGoods.Service;

import java.util.List;
import com.serve.DicksSportingGoods.Dto.DepartmentDto;

public interface DepartmentService {
	
	
	public List<DepartmentDto> findAll();

	String saveDepartments(List<DepartmentDto> department);

}
