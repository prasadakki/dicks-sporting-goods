package com.serve.DicksSportingGoods.Model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

//@Entity
@Table(name = "productitem")
public class ProductItem implements Serializable {
private static final long serialVersionUID = 1L;

@Id
private int productId;

@Column(name = "brand")
private String brand;

@Column(name = "price")
private int price;

public int getProductId() {
	return productId;
}

public void setProductId(int productId) {
	this.productId = productId;
}

public String getBrand() {
	return brand;
}

public void setBrand(String brand) {
	this.brand = brand;
}

public int getPrice() {
	return price;
}

public void setPrice(int price) {
	this.price = price;
}



	

}
