package com.serve.DicksSportingGoods.Dto;

public class DepartmentDto {
	private int departmentId;
	private String footware;
	private String apparel;
	private String outerware;
	private String accessories;
	private String fanShop;
	private String featured;
	private String clearance;

	public int getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}

	public String getFootware() {
		return footware;
	}

	public void setFootware(String footware) {
		this.footware = footware;
	}

	public String getApparel() {
		return apparel;
	}

	public void setApparel(String apparel) {
		this.apparel = apparel;
	}

	public String getOuterware() {
		return outerware;
	}

	public void setOuterware(String outerware) {
		this.outerware = outerware;
	}

	public String getAccessories() {
		return accessories;
	}

	public void setAccessories(String accessories) {
		this.accessories = accessories;
	}

	public String getFanShop() {
		return fanShop;
	}

	public void setFanShop(String fanShop) {
		this.fanShop = fanShop;
	}

	public String getFeatured() {
		return featured;
	}

	public void setFeatured(String featured) {
		this.featured = featured;
	}

	public String getClearance() {
		return clearance;
	}

	public void setClearance(String clearance) {
		this.clearance = clearance;
	}

	@Override
	public String toString() {
		return "DepartmentDto [departmentId=" + departmentId + ", footware=" + footware + ", apparel=" + apparel
				+ ", outerware=" + outerware + ", accessories=" + accessories + ", fanShop=" + fanShop + ", featured="
				+ featured + ", clearance=" + clearance + "]";
	}

}
