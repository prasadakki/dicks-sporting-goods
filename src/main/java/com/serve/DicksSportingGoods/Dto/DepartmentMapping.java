package com.serve.DicksSportingGoods.Dto;

import java.util.List;

import com.serve.DicksSportingGoods.Model.Department;

public interface DepartmentMapping {

	List<DepartmentDto> convertEntityToDto(List<Department> department);

	List<Department> convertDtoIntoEntity(List<DepartmentDto> departmentDto);

}
