package com.serve.DicksSportingGoods;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DicksSportingGoodsApplication {

	public static void main(String[] args) {
		SpringApplication.run(DicksSportingGoodsApplication.class, args);
	}

}

