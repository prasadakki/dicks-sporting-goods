package com.serve.DicksSportingGoods.Service;

import java.util.ArrayList;
import java.util.List;

import com.serve.DicksSportingGoods.Dto.DepartmentDto;
import com.serve.DicksSportingGoods.Model.Department;

public class DepartmentFixtures {
	
	public static List<DepartmentDto> assignAllEmlements(){
		
		List<DepartmentDto> list = new ArrayList<DepartmentDto>();
		
		DepartmentDto d = new DepartmentDto();
		
		d.setAccessories("acc");
		d.setApparel("app");
		d.setClearance("clearance");
		d.setDepartmentId(1);
		d.setFanShop("fanShop");
		d.setFeatured("featured");
		d.setFootware("footware");
		d.setOuterware("outerware");
		
		return list;
	}

}
