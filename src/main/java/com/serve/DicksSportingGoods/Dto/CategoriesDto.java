package com.serve.DicksSportingGoods.Dto;

public class CategoriesDto {

	private int categoryId;
	private String name;
	private int parentCategoryId;
	private int departmentId;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	public int getParentCategoryId() {
		return parentCategoryId;
	}
	public void setParentCategoryId(int parentCategoryId) {
		this.parentCategoryId = parentCategoryId;
	}
	public int getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}
	
	
	@Override
	public String toString() {
		return "CategoriesDto [categoryId=" + categoryId + ", name=" + name + ", parentCategoryId=" + parentCategoryId
				+ ", departmentId=" + departmentId + "]";
	}
	
	
}
