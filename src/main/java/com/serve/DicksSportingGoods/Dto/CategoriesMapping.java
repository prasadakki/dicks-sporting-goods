package com.serve.DicksSportingGoods.Dto;

import java.util.List;

import com.serve.DicksSportingGoods.Model.Categories;

public interface CategoriesMapping {

	List< CategoriesDto> convertEntityToDto(List< Categories>  categories);

	List< Categories> convertDtoIntoEntity(List< CategoriesDto>  categoriesDto);

}
