package com.serve.DicksSportingGoods.Dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.serve.DicksSportingGoods.Model.Department;
@Repository
public interface DepartmentRepository extends CrudRepository<Department,Integer>{

}
