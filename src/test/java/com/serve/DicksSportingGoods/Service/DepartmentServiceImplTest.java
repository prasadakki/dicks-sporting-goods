package com.serve.DicksSportingGoods.Service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.serve.DicksSportingGoods.Dto.DepartmentDto;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DepartmentServiceImplTest {

	
	@Autowired
	private DepartmentServiceImpl service;
	
	
	@Test
	public void toGetAllUsers() {
		
		List<DepartmentDto> Department = service.findAll();
		
		assertNotNull(Department);
		assertEquals(9, Department.size());
	}
	
	@Test
	public void checkWhetherDepartmentsAreSaved() {
		
		String Department = service.saveDepartments(DepartmentFixtures.assignAllEmlements());
		
		assertEquals("Sucessfully Saved", Department);
	}
}
