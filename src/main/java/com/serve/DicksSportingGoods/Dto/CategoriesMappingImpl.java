package com.serve.DicksSportingGoods.Dto;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.serve.DicksSportingGoods.Model.Categories;
import com.serve.DicksSportingGoods.Model.Department;


@Component
public abstract class CategoriesMappingImpl implements CategoriesMapping {
	
	@Override
	public List<CategoriesDto> convertEntityToDto(List<Categories> categories){
		
		List<CategoriesDto> CategoriesDtoList = new ArrayList<CategoriesDto>();
		
		if (categories != null) {

			for (Categories Category : categories) {
				
				CategoriesDto CategoriesDto = new CategoriesDto();
				
				CategoriesDto.setCategoryId(Category.getCategoryId());
				CategoriesDto.setName(Category.getName());
				CategoriesDto.setParentCategoryId(Category.getParentCategoryId());
				CategoriesDto.setDepartmentId(Category.getDepartmentId());
				
				
				
				//set the value
				
				CategoriesDtoList.addAll(CategoriesDtoList);
			}
		}

		return CategoriesDtoList;
		
	}
	
	@Override
	public  List<Categories> convertDtoIntoEntity(List<CategoriesDto> categoriesDto){
		
		List<Categories> categoriesDtoList = new ArrayList<Categories>();
		
		if(categoriesDto != null) {
			
			for(CategoriesDto Category : categoriesDto) {
				
				Categories CategoriesDto = new Categories();
				
				CategoriesDto.setCategoryId(Category.getCategoryId());
				CategoriesDto.setName(Category.getName());
				CategoriesDto.setParentCategoryId(Category.getParentCategoryId());
				CategoriesDto.setDepartmentId(Category.getDepartmentId());
				
				
				categoriesDtoList.addAll(categoriesDtoList);
			}
		}
		
		return categoriesDtoList;
	}

}
