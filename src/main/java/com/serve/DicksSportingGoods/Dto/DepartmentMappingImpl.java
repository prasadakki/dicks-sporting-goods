package com.serve.DicksSportingGoods.Dto;

import java.util.ArrayList;

import java.util.List;

import org.springframework.stereotype.Component;

import com.serve.DicksSportingGoods.Model.Department;

@Component
public class DepartmentMappingImpl implements DepartmentMapping{
	
	
	@Override
	public List<DepartmentDto> convertEntityToDto(List<Department> department){
		
		List<DepartmentDto> departmentDtoList = new ArrayList<DepartmentDto>();
		
		if (department != null) {

			for (Department depart : department) {
				
				DepartmentDto departmentDto = new DepartmentDto();
				
				departmentDto.setDepartmentId(depart.getDepartmentId());
				departmentDto.setAccessories(depart.getAccessories());
				departmentDto.setApparel(depart.getApparel());
				departmentDto.setClearance(depart.getClearance());
				departmentDto.setFanShop(depart.getFanShop());
				departmentDto.setFeatured(depart.getFeatured());
				departmentDto.setFootware(depart.getFootware());
				departmentDto.setOuterware(depart.getOuterware());
				
				//set the value
				
				departmentDtoList.add(departmentDto);
			}
		}

		return departmentDtoList;
		
	}
	
	@Override
	public  List<Department> convertDtoIntoEntity(List<DepartmentDto> departmentDto){
		
		List<Department> departmentDtoList = new ArrayList<Department>();
		
		if(departmentDto != null) {
			
			for(DepartmentDto depart : departmentDto) {
				
				Department department = new Department();
				
				department.setAccessories(depart.getAccessories());
				department.setApparel(depart.getApparel());
				department.setClearance(depart.getClearance());
				department.setDepartmentId(depart.getDepartmentId());
				department.setFanShop(depart.getFanShop());
				department.setFeatured(depart.getFeatured());
				department.setFootware(depart.getFootware());
				department.setOuterware(depart.getOuterware());
				
				departmentDtoList.add(department);
			}
		}
		
		return departmentDtoList;
	}

}
