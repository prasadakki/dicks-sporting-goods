package com.serve.DicksSportingGoods.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.serve.DicksSportingGoods.Dao.CategoryRepository;
import com.serve.DicksSportingGoods.Dto.CategoriesDto;
import com.serve.DicksSportingGoods.Dto.CategoriesMapping;
import com.serve.DicksSportingGoods.Model.Categories;

@Service
public class CategoryServiceImpl implements CategoryService {
	
		@Autowired
		private CategoryRepository repo;

		@Autowired
		private CategoriesMapping mapping;

		@Override
		public List<CategoriesDto> findAll() {

			List<Categories> category = (List<Categories>) repo.findAll();

			List<CategoriesDto> categoriesDtoList = mapping.convertEntityToDto(category);

			return categoriesDtoList;
		}

		@Override
		public String saveCategories(List<CategoriesDto> categories) {

			List<Categories> categories1 = mapping.convertDtoIntoEntity(categories);
			
			Iterable<Categories> it = categories1;
			
			if (it == null) {
				return "UnSucessfully";
			} else {
				return "Sucessfully Saved";
			}
		}


}
